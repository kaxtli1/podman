

# Este es el tipo de problemas con experimental
	- libsemanage1 de stable es muy viejo, pero no existe en testing ni experimental


root@9077a83a8938:~ # apt install podman/experimental libsemanage-common/testing libselinux1/testing apt/testing
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Selected version '4.3.1+ds1-2' (Debian:experimental [amd64]) for 'podman'
Selected version '3.4-1' (Debian:testing [all]) for 'libsemanage-common'
Selected version '3.4-1+b3' (Debian:testing [amd64]) for 'libselinux1'
Selected version '2.5.4' (Debian:testing [amd64]) for 'apt'
Selected version '2.5.4' (Debian:testing [amd64]) for 'libapt-pkg6.0' because of 'apt'
Selected version '1.10.1-2' (Debian:testing [amd64]) for 'libgcrypt20' because of 'libapt-pkg6.0'
Selected version '12.2.0-9' (Debian:testing [amd64]) for 'libstdc++6' because of 'libapt-pkg6.0'
Selected version '1.5.2+dfsg-1' (Debian:testing [amd64]) for 'libzstd1' because of 'libapt-pkg6.0'
Selected version '3.7.8-4' (Debian:testing [amd64]) for 'libgnutls30' because of 'apt'
Selected version '2:6.2.1+dfsg1-1.1' (Debian:testing [amd64]) for 'libgmp10' because of 'libgnutls30'
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 libsemanage1 : Depends: libsemanage-common (= 3.1-1) but 3.4-1 is to be installed
E: Unable to correct problems, you have held broken packages.
root@9077a83a8938:~ # apt install podman/experimental libsemanage-common/testing libselinux1/testing apt/testing libsemanage1/testing
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
E: Release 'testing' for 'libsemanage1' was not found
root@9077a83a8938:~ # apt search libsemanage1
Sorting... Done
Full Text Search... Done
libsemanage1/stable,now 3.1-1+b2 amd64 [installed,automatic]
  SELinux policy management library

libsemanage1-dev/stable 3.1-1+b2 amd64
  Header files and libraries for SELinux policy manipulation

# Errar: fork: retry: Resource temporarily unavailable
 

Este error sucede con --pids-limit=-1 en pods, si se ejecuta solo funciona, si se ejecuta en pods falla.
Este parametro lo necesite para Xorg, pero no se si en contenedores sea indispensable ..

	podman run -d --replace --privileged --name podman-01 -v var:/var/:suid,exec,dev -v /root/podman-01:/root/podman-01  -v /srv/podman/:/var/lib/containers --pids-limit=-1 --tz local kaxtli/podman:alpha
	 --pids-limit=-1  # en pods causa: bash: fork: retry: Resource temporarily unavailable


# Pods: apt-cacher-ng


 podman run --pod podman4-pod -d --name podman4-demo --replace --privileged -v var-tmp:/var/:suid,exec,dev -ti -v /root/kaxtli/:/root/kaxtli --tz local  kaxtli/podman4-demo:latest 
 podman run --pod podman4-pod -d --name apt-cacher --replace kaxtli/apt-cacher-ng:latest 
 podman pod create --name podman4-pod

# DNS doesn't works

The port 53 is taken by `systemd-resolved` 

	lsof -i -L -P


One posible solution is disable DNS stub for DNSSEC in `systemd-resolved`

	sed -i 's/#DNSStubListener=yes/DNSStubListener=no/' /etc/systemd/resolved.conf

Other **better solution** is change the `aardvark-dns` port


	echo -e "[network]\ndns_bind_port=5553" >> /etc/containers/containers.conf



